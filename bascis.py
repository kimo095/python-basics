# numbers
a = 5 
b = 3
# basics
# additional 
print(a+b)
# substractio 
print(a-b)
# divistion 
print(a/b)
# multiplication 
print(a*b)
# power of 
print(a**b)

# strings

greeting = "hello world  , I`m abdelkareem" 
print(greeting)

# indexing
print(greeting[2])
print(greeting[:2])
print(greeting[2:])
print(greeting[2:8])
print(greeting[:])
# step size equal to 1
print(greeting[::1])
# step size equal to 2
print(greeting[::2])

# some python methods
x = greeting.split('o')
y = greeting.upper()
print (x)
print (y)

# print formating
x = "Item One:{x} Item Two:{y}".format(x="dog" , y="cat")
print(x)

# strings are immuatable , in which I can not change after assignment =/> greeting[0] = 'd'

# lists => arrays

my_list = [ 'a' , 'b' , 'c' , 'd']

print(len(my_list))

# on the other hand list are not immuatable and can be modified

my_list[0]='new item'

print(my_list)

# indexing
print(my_list[2])
print(my_list[:2])
print(my_list[2:])
print(my_list[2:4])
print(my_list[:])
print(my_list[::2])

# some methods used with lists

# add an item to the end of the list
my_list.append(['f' , 'g'])
print(my_list)
# how to make new list to be a part of the previous list
my_list.extend(['f' , 'g'])
print(my_list)
# how to remove the last item from the list 
last_item = my_list.pop() # it remove the last item by default
print(my_list)
print(last_item)
# how to remove the specific item from the list 
third_item = my_list.pop(2) # it remove the last item by default
print(my_list)
print(third_item)
# other useful methods for the list
# reverse the list items orders
my_list.reverse()
print(my_list)
# another important method is sort
lists = [ 3 , 5 , 3 , 6 , 8 , 27]

lists.sort()
print(lists)


# indexing nested lists
nested_lists = [ 1 , 2 , 3 , 4 , ['a' , 'b' , 'c']]
print(nested_lists[4][2])

# lists comperhentions => matrix
matrix = [[1,2,3],[4,5,6],[7,8,9]]
first_col = [row[0] for row in matrix]
print(first_col)

# dictories => objects in javascript
# the different between dictonries in python and object in javascript , is that in dictionries I can not able to add methods
my_stuff = {"key1":123 , "key2":"value2" , "key3":{"123":[1,2,3]}}
print(my_stuff["key3"]["123"])

# I can reassign the dictories easily
my_food = {"bfast":"eggs" , "lunch":"pizza" , "dinner":"yogurt"}
print(my_food)
my_food["lunch"]="burger"
print(my_food)

# boolean data
# True and False

# tuples
# is simialr to the list but it is immuatable , I can not change or edit element inside it

t = (1 , 3, 'a' , 'b' , 4 , False , True)
print(t[0])

# sets
# it is simialr to the dictionries in way that my output will be in a form of {}
# it only print a unique element
X = set()

X.add(1)
X.add(2)
X.add(3)
X.add(4)
X.add(5)
X.add(5)
X.add(5)
X.add(5)
X.add(5)
X.add(5)
X.add(5)

print(X)

Y = set([1,2,3,4,5,6,6,4,5,3,2,1,1,1,1])

print(Y)

Z = set(['a','a','b','b','v','c','c'])
print(Z)

e ='django'

print(e[::-1])

d3 = {'k1':[{'nest_key':['this is deep' , ['hello']]}]}


r = d3['k1'][0]['nest_key'][1][0]

print(r)