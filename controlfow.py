# greater than and equal
# x > y  x>=y 
# less than and equal
# x < y   x<=y
# equality
# 1 == 1 True
# 1 == '1' False
# inequality
# 1 != 2
# logical operators
# (1 > 2)  and (2<3)  #and
# (1 > 2)  or (2<3)  #or
# #multiple logical operators
# (1 > 2)  or (2<3)  or (1>2)

# if statement

if 1<2:
    print('first block')
    if 20<3:
        print("second block")

# if & else statement

if 1<.2:
    print("true")
elif 3 == 3:
    print('elif ran')
else:
    print("false")

# for loop

seq = [ 1 , 2 , 3 , 4, 5,6]

for item in seq:
    # code here
    print("hello")

# dictionary case

dictionary = {"sam":1 , "Frank":2 , "Dan":3} 

for d in dictionary:
    print(d)
    print(dictionary[d])

# tuples case

my_pairs = [(1,2) , (3,4) , (5,6)]

for tuples in my_pairs:
    print(tuples)

# other case

for (tuple1 , tuple2) in my_pairs:
    print(tuple1)
    print(tuple2)

# while loop

i = 1

while i < 5:
    print("i is equal to :{}".format(i))
    i= i+1

# range function , the first parameter of the range function is the start point while the second parameter is the end point but not including
# it is very important because it will save my computer memory
r = list(range(0,6))
print(r)
x = list(range(0,21,2)) #here in this case my step is equal to 2
print(x)

for item in range(11):
    print(item)


# list comperhention

list_num = [ 1 , 2 , 3 , 4 , 5]

out = []

for num in list_num:
    out.append(num**2)

print(out)

# other cool way
res = [num**2 for num in list_num]
print(res)