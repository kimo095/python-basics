# s = "Global variable"

# def func():
#    global s
#    s= 60
#    print(s)
# func()
# print(s)

s = "Global variable"

def func():
   mylocal = 10
   print(locals())
func()

def hello(name='kimo'):
   return "hello "+name

print(hello())
greeting = hello
print(greeting())

def hi(name='abdelkareem'):
   print("The hi() function has been run!")
   def greet():
      return "this string inside greet()"
   def welcome():
      return "this string inside welcome!"
   
   if name == "abdelkareem":
      return greet
   else:
      return welcome
#    print(greet())
#    print(welcome())
#    print("end of hello")
# #greet() I can call the function greet here because it is outside the scope
# hi()
x = hi()
print(x())

#passing function inside another function
def HELLO():
   return "Hi kiko!"
def other(func):
   print("HELLO")
   print(func())
other(HELLO)

####################################### DECORATOR #########################################
def new_decorator(func):
   def wrap_func():
      print("CODE HERE BEFORE EXECUTING FUNC")
      func()
      print("FUNC() HAS BEEN CALLED")
   return wrap_func
# def func_needs_decorator():
#    print("THIS FUNCTTION IS IN NEED OF A DECORATOR")

# func_needs_decorator = new_decorator(func_needs_decorator)
# func_needs_decorator()
#another way to apply the same logic above
@new_decorator
def func_needs_decorator():
   print("THIS FUNCTTION IS IN NEED OF A DECORATOR")

func_needs_decorator()
