def my_function(param1='default'):
    # documting your function
    """
    This is the docstring for your function

    """
    print("This is my first python function here {} ...".format(param1))

my_function()

# function with using return keywords

def hello():
    return "hello"


result = hello()

print(result)

def add_nums(num1,num2):
    if type(num1)==type(num2)==type(10):
        return num1+num2
    else:
        return "sorry we only consider interger here ....."

r = add_nums("2",3)
print(r)
    

# lambda function expression

num_list = [ 1 , 2 , 3 , 4 , 5 , 6 , 7 , 30 , 34]

def even_function(num):
    return num%2 ==0

# using filter function

even = filter(even_function , num_list)

print(list(even))

# use lambda function 

even_two = filter(lambda num:num%2 == 0 , num_list)
print(list(even_two))

# please write a real example for using method like split
tweet = "I love sport #sports"

result = tweet.split('#')[1]

print(result)

# in
print('x' in  [ 1 , 2 , 4 , 'x'])


# write a function that return true if the sequence of a number in a list is true
def true_order(first_list):
    for index in range(1, len(first_list) - 1):
        if first_list[index] - first_list[index - 1] == first_list[index + 1] - first_list[index]:
            print("True")
        else:
            print("False")
# other solution

def array_check(nums):
    for i in range(len(nums)-2):
        if nums[i]==1 and nums[i+1]==2 and nums[i+2]==3:
            return True
    return False
# --------------------------------------
# Test case
first_list = [2, 2, 3, 1, 2, 4 , 6 ,6]
true_order(first_list)

def string_bits(str):
    return str[::2]
result = string_bits("hi")
print(result)

# other solution

def stringBits(mystring):
    result = ""

    for i in range(len(mystring)):
        if i%2 == 0:
            result = result + mystring[i]
    return result
# ----------------------------------
def check_endstring(first_str, second_str):
    if second_str in first_str:
        return True
    else:
        return False

res = check_endstring("abdlekareem", "a")
print(res)  # Output: True


# other solution

def end_other(a,b):
    a = a.lower()
    b = b.lower()

    return a[-(len(b)):] == b or a == b[-len(a):]



#write a function that return the double of the string , e.g hi => hhii

def doubleChar(mystring):
    result = ''
    for char in mystring:
        result += char*2
    return result 


# write a code to return the number of even intergers in the given array

def count_evens(nums):
    count = 0

    for element in nums:
        if element % 2 == 0:
            count += 1
    return nums