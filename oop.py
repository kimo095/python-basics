class Dog():
    # Class object attribute
    species = "mammal"

    def __init__(self,breed,name):
        self.breed = breed
        self.name = name
    #self refeer to the class Dog

mydog = Dog(breed = "Lab" , name = "sammy")
print(mydog.breed)
print(mydog.name)
print(mydog.species)

# add method inside my class

class Circle():
    pi = 3.14

    def __init__(self,radius=1):
        self.radius = radius
    
    def area(self):
        return self.radius*self.radius * Circle.pi
    
    # define a function that can reset the radius

    def set_radius(self,new_radius):
        self.radius = new_radius
    

myc = Circle(5)
print(myc.radius)
print(myc.set_radius(999))
print(myc.area())


# inhertiance concepts
# in this case the class of one method is used for another method , the class that was used is called base class
class Animal():#base class

    def __init__(self):
        print("ANIMAL CREATED")
    def whoAmI(self):
        print("Animal")   
    def eat(self):
        print('EATING') 


class Dog(Animal):
    def __init__(self):
        # Animal.__init__(self)
        print("DOG CREATED")
    
    def bark(self):
        print("WOOF")

    # I can also overwrite the previous method in the main class
    def eat(self):
        print("DOG EATING")

#in this case i succsuffly inherent dog class from the main class which is an animal , eveb if i comment out the [# Animal.__init__(self)] but still whAmI
#and eat , appear in my other class
mydog = Dog()
mydog.whoAmI()
mydog.eat()
mydog.bark()


# Special methods
# def __str__(self) and it is very important because otherwise i will get location at the memory as the output
# def __len__(self) to get the length
# def __del__(self) to destroy the object
class Book():
    def __init__(self , title , author , pages):
        self.title = title
        self.author = author 
        self.pages = pages
    def __str__(self):
        return "Title: {}, Author:{}, Pages:{}".format(self.title , self.author , self.pages)
    def __len__(self):
        return self.pages
    def __del__(self):
        print("A book is destroyed")

b = Book("python" , "kareem" , 1000)

print(b)
print(len(b))