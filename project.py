# write a code for a game in which the player will enter 3-digits number , then according to this the computer will provide a feedback accordingly
# if there is a match it will return a match , if it is close it will return close number , and finally if there is no match it will return nope
# steps to solve the game

# 1- get guess from the user ; 2- generate the computer code ; 3- generate the clue ; 4- run game logic
import random
# get a guess

def get_guess():
    return list(input("What is your guess"))

# generate the random computer code with 3 digits

def generate_code():
    digits = [str(num) for num in range(10)]

    #shuffle the digits
    random.shuffle(digits)

    return digits[:3]



#  generate the clue 

def generate_clues(code,user_guess):

    if user_guess == code:
        return "CODE CRACKED!"
    clues = []

    for ind,num in enumerate(user_guess):
        if num == code[ind]:
            clues.append("match")
        elif num in code:
            clues.append("Close")
    
    if clues == []:
        return ["Nope try again baby"]
    else:
        return clues

# run the game logic

print(" Welcome Code Breaker!")

secret_code = generate_code()

clue_report = []

while clue_report != "CODE CRACKED!":
    guess = get_guess()

    clue_report = generate_clues(guess,secret_code)
    print("here is the result of your guess:")
    for clue in clue_report:
        print(clue)




# run the logic 
x = get_guess()
print(x)