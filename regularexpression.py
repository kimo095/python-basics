import re

patterns = [ 'term1' , 'term2' , 'term3']

text = 'This is a string with term1, not the other!'

for pattern in patterns:
    print("I'm searching for :" + pattern)

    if re.search(pattern,text):
        print('Match!')
    else:
        print('No Match')

textTwo = 'this'

match = re.search('this' , textTwo)
print(match.start()) # it will return the index when it is match 

#regular expression also has split method
split_term = '@'
email = 'user@gmail.com'

print(re.split(split_term,email))

#I can use it also to find the match
print(re.findall('match', 'test phrase match in match middle'))

def multi_re_find(patterns,phrase):

    for pat in patterns:
        print("Searching for pattern {}".format(pat))
        print(re.findall(pat,phrase))
        print('\n')

# test_phrase = 'sdsd..sssddd..sdddsddd..dsds..dssssss...sdddddd'
# test_patterns = ['sd+'] followed by one or more d
# test_patterns = ['sd']
# test_patterns = ['sd*'] followed by zeros or more 
# test_patterns = ['sd?'] followed by either zero or one
# test_patterns = ['sd{3}'] # here you can defined the actual number
# test_patterns = ['s[sd]+'] #followed by one or more s or one or more d

# test_phrase = 'This is a string! But is has punctuation. How can we remove it?'
# test_patterns = ['[a-z]+'] #give you all the lower letters
# test_patterns = ['[A-Z]+'] #give you all the upper letters
# test_patterns = ['[^!.?]+'] # kind of filterations

test_phrase = 'This is a string with numbers 12312 and symbol #hashtag'
# test_patterns = [r'\d+'] #give you all the numbers,digits
# test_patterns = [r'\D+'] #give you all the non-digits
# test_patterns = [r'\s+'] #give you all the list of white spaces
# test_patterns = [r'\S+'] #give you all the list of the non-white spaces
# test_patterns = [r'\w+'] #give you all the list of the alpha numeric
test_patterns = [r'\W+'] #give you all the list of the non-alpha numeric

multi_re_find(test_patterns,test_phrase)