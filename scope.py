x = 25 # global varaiable

def my_func():
    x = 50
    return x

# if i print x here i will get the value of global variable => x = 25
print(x)
# if i print the function which change the variable inside it in the local function scope it will return new x value => x = 50
print(my_func())
# if i run the function first and then print x , I will get x = 25 because the x is only considered for local 
my_func()
print(x)

# enclosing function locals
name = 'This is a global name!'

def greet():
    name='sammy'

    def hello():
        print("hello" + " " + name)

    hello()
greet()

# In the example above , the variable name is considered a global variable in which the first print result here will be [hello sammy ] 
# but if i comment out the varaible name = "sammy" then in this case i will go to the next level which in this case i will print the global varaible 
# name = "This is a global name"


y = 50 

def func(y):
    print('y is:',y)
    y = 1000
    print('local y changed to :' , y)
func(y)
print(y)


z = 20
def fun_z():
    global z #always avoid using global key word because this my missed up other varaible globally within your code
    z = 1200

print("before function call, z is :" , z)
fun_z()
print("After function call, z is :" , z)

# using return without using global
v = 20
def fun_v():
    v = 1200
    return v

print("before function call, v is :" , v)
v = fun_v()
print("After function call, v is :" , v)